const http = require('http');
const port = process.env.PORT || 3000;

const requestHandler = async (req, res) => {
    let obj = {
        message:"Hello from rand server",
        random: Math.random(),
        time: new Date(),
        remoteAddress:req.connection.remoteAddress,
        remotePort:req.connection.remotePort,
        host:req.headers.host,
        localAddress:req.connection.localAddress
    }
    res.setHeader('Content-Type', 'application/json');
    res.statusCode = 200;
        
    res.end(JSON.stringify(obj, null, 2));
}

const server = http.createServer(requestHandler);
server.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err);
    }
    console.log(server.address().address);
    console.log(`server is listening on ${port}`);
})
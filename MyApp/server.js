const http = require('http');
const port = process.env.PORT || 3000;
const randPORT = process.env.RAND_PORT || 3333;
const randDomain = process.env.RAND_DOMAIN || 'localhost';
const requestHandler = async (req, res) => {
    console.log(req.url);

    let obj = {
        random: Math.random(),
        time: new Date(),
        info: {
            remoteAddress:req.connection.remoteAddress,
            remotePort:req.connection.remotePort,
            localAddress:req.connection.localAddress,
            localPort:req.connection.localPort,
            host:req.headers.host
        }
    }
    obj.test = await getRandServerInfo();
    res.setHeader('Content-Type', 'application/json');
    res.statusCode = 200;
        
    res.end(JSON.stringify(obj, null, 2));
}

function getRandServerInfo(){
    return new Promise((resolve, reject) =>{
        http.get('http://' + randDomain + ':' + randPORT, (resp) => {
        let data = '';

        resp.on('data', (chunk) => {
            data += chunk;
        });

        resp.on('end', () => {
            resolve(JSON.parse(data));
        });

        }).on("error", (err) => {
            console.log("Error: " + err.message);
            resolve(err.message);
        });
    });
}

const server = http.createServer(requestHandler);
server.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err);
    }
    console.log(server.address().address);
    console.log(`server is listening on ${port}`);
})